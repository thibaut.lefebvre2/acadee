//SPDX-License-Identifier: Unlicensed
pragma solidity ^0.8.18;

contract DocumentSigner {
    event DocumentSigned(bytes32 indexed documentId, address indexed signer);
    event DocumentPaused(bytes32 indexed documentId);
    event DocumentResumed(bytes32 indexed documentId);
    event DocumentArchived(bytes32 indexed documentId);
    event DocumentUploaded(bytes32 indexed documentId, address indexed uploader);

    struct Document {
        string content;
        bool paused;
        bool archived;
        address[] signatures;
        address uploader;
    }

    mapping(bytes32 => Document) private documents;

    modifier documentExists(bytes32 _documentId) {
        require(bytes(documents[_documentId].content).length > 0, "Document not found");
        _;
    }

    modifier notPaused(bytes32 _documentId) {
        require(!documents[_documentId].paused, "Document signing paused");
        _;
    }

    modifier notArchived(bytes32 _documentId) {
        require(!documents[_documentId].archived, "Document is already archived");
        _;
    }

    function importDocument(string calldata _content) external {
        bytes32 documentId = keccak256(abi.encodePacked(msg.sender, _content));
        require(bytes(documents[documentId].content).length == 0, "Document already exists");
        documents[documentId].content = _content;
        documents[documentId].uploader = msg.sender;
        emit DocumentUploaded(documentId, msg.sender);
    }

    function getDocument(bytes32 _documentId) external view documentExists(_documentId) notPaused(_documentId) returns(Document memory) {
        return documents[_documentId];
    }

    function signDocument(bytes32 _documentId) external documentExists(_documentId) notPaused(_documentId) {
        documents[_documentId].signatures.push(msg.sender);
        emit DocumentSigned(_documentId, msg.sender);
    }

    function isDocumentSigned(bytes32 _documentId) external view documentExists(_documentId) returns (bool) {
        for(uint i = 0; i < documents[_documentId].signatures.length; i++){
            if(documents[_documentId].signatures[i] == msg.sender) return true;
        }
        return false;
    }

    function pauseSigning(bytes32 _documentId) external documentExists(_documentId) notArchived(_documentId) {
        documents[_documentId].paused = true;
        emit DocumentPaused(_documentId);
    }

    function resumeSigning(bytes32 _documentId) external documentExists(_documentId) {
        documents[_documentId].paused = false;
        emit DocumentResumed(_documentId);
    }

    function getAddressCount() private view returns (uint256) {
        return block.number / 65536;
    }
}
