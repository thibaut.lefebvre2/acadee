require("@nomicfoundation/hardhat-toolbox");
require("@nomiclabs/hardhat-etherscan")
require('hardhat-abi-exporter');
require("dotenv").config()
const {INFURA, PRIVATE_KEY, ETHERSCAN_API} = process.env

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: {
    version: "0.8.18",
    settings: {
      optimizer: {
        enabled:true,
        runs: 200,
      }
    }
  },
  paths: {
    artifacts: './artifacts',
  },
  defaultNetwork: 'mumbai',
  networks: {
    mumbai: {
      url: INFURA,
      accounts: [`0x${PRIVATE_KEY}`],
      gasPrice: 20000000000, // 20 gwei
      gasLimit: 2000000,
      nonce: 1
    }
  },
  etherscan: {
    apiKey: ETHERSCAN_API
  }

};
