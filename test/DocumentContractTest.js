// Importer les librairies nécessaires
const { expect } = require("chai");
const { ethers } = require("hardhat");

// Commencer le bloc de tests
describe("DocumentSigner", function () {

    // Créer des variables globales utilisées dans les tests
    let contract;
    let owner;

    // Déployer un nouveau contrat avant chaque test
    beforeEach(async function () {
        [owner] = await ethers.getSigners();

        const DocumentRegistry = await ethers.getContractFactory("DocumentSigner");
        contract = await DocumentRegistry.deploy();
        await contract.deployed();
    });

    // Import doucment
    describe("Import and Sign", function () {
        it("Should import a new document", async function () {
            const content = "This is a test document.";

            const tx = await contract.connect(owner).importDocument(content);
            const receipt = await tx.wait();

            expect(receipt.events[0]).to.be.an("object");
            expect(receipt.events[0].event).to.equal("DocumentUploaded");
            expect(receipt.events[0].args.uploader).to.equal(owner.address);
        });

        // Cannot import document with same content
        it("Should not allow importing a document with the same content twice", async function () {
            const content = "This is a test document.";

            // Importer un document
            await contract.connect(owner).importDocument(content);

            // Essayer d'importer le même document à nouveau
            await expect(contract.connect(owner).importDocument(content)).to.be.revertedWith("Document already exists")
        });

        it("Should sign a document", async function () {
            const content = "This is a test document.";

            // Importer un document
            const tx1 = await contract.connect(owner).importDocument(content);
            const receipt = await tx1.wait();

            expect(receipt.events[0]).to.be.an("object");
            expect(receipt.events[0].event).to.equal("DocumentUploaded");
            expect(receipt.events[0].args.uploader).to.equal(owner.address);

            // Signer le document
            const tx2 = await contract.connect(owner).signDocument(receipt.events[0].args.documentId);
            const receipt2 = await tx2.wait();

            expect(receipt2.events[0]).to.be.an("object");
            expect(receipt2.events[0].event).to.equal("DocumentSigned");
            expect(receipt2.events[0].args.signer).to.equal(owner.address);
        });
    });

    // Document can be paused
    describe("Pause and Resume Signing", function () {
        it("Should pause document signature", async function () {
            const content = "This is a test document.";

            // Import document
            const tx1 = await contract.connect(owner).importDocument(content);
            const receipt = await tx1.wait();

            // Pause document
            const tx2 = await contract.connect(owner).pauseSigning(receipt.events[0].args.documentId);
            const receipt2 = await tx2.wait();

            expect(receipt2.events[0]).to.be.an("object");
            expect(receipt2.events[0].event).to.equal("DocumentPaused");
        });

        it("Should resume document signature", async function () {
            const content = "This is a test document.";

            // Import document
            const tx1 = await contract.connect(owner).importDocument(content);
            const receipt = await tx1.wait();

            // Pause document
            const tx2 = await contract.connect(owner).pauseSigning(receipt.events[0].args.documentId);
            const receipt2 = await tx2.wait();

            expect(receipt2.events[0]).to.be.an("object");
            expect(receipt2.events[0].event).to.equal("DocumentPaused");

            // Resume document
            const tx3 = await contract.connect(owner).resumeSigning(receipt.events[0].args.documentId);
            const receipt3 = await tx3.wait();

            expect(receipt3.events[0]).to.be.an("object");
            expect(receipt3.events[0].event).to.equal("DocumentResumed");

        });
    })
})