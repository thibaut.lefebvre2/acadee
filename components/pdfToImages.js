import pdfjsLib from 'pdfjs-dist/es5/build/pdf';
import sharp from 'sharp';

export async function convertPdfToImages(pdfBuffer) {
    const images = [];
    const pdf = await pdfjsLib.getDocument({ data: pdfBuffer }).promise;

    for (let pageNum = 1; pageNum <= pdf.numPages; pageNum++) {
        const page = await pdf.getPage(pageNum);
        const viewport = page.getViewport({ scale: 1 });

        const canvas = document.createElement('canvas');
        canvas.width = viewport.width;
        canvas.height = viewport.height;
        const context = canvas.getContext('2d');

        await page.render({ canvasContext: context, viewport }).promise;

        const imageBuffer = await new Promise((resolve, reject) => {
            canvas.toBlob(async (blob) => {
                const buffer = await blob.arrayBuffer();
                resolve(new Buffer(buffer));
            }, 'image/png');
        });

        const image = await sharp(imageBuffer).png().toBuffer();
        images.push(image);
    }

    return images;
}
