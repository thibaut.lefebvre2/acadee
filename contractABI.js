export const contractAbi = [
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "bytes32",
                "name": "documentId",
                "type": "bytes32"
            }
        ],
        "name": "DocumentArchived",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "bytes32",
                "name": "documentId",
                "type": "bytes32"
            }
        ],
        "name": "DocumentPaused",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "bytes32",
                "name": "documentId",
                "type": "bytes32"
            }
        ],
        "name": "DocumentResumed",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "bytes32",
                "name": "documentId",
                "type": "bytes32"
            },
            {
                "indexed": true,
                "internalType": "address",
                "name": "signer",
                "type": "address"
            }
        ],
        "name": "DocumentSigned",
        "type": "event"
    },
    {
        "anonymous": false,
        "inputs": [
            {
                "indexed": true,
                "internalType": "bytes32",
                "name": "documentId",
                "type": "bytes32"
            },
            {
                "indexed": true,
                "internalType": "address",
                "name": "uploader",
                "type": "address"
            }
        ],
        "name": "DocumentUploaded",
        "type": "event"
    },
    {
        "inputs": [
            {
                "internalType": "string",
                "name": "_content",
                "type": "string"
            }
        ],
        "name": "importDocument",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "bytes32",
                "name": "_documentId",
                "type": "bytes32"
            }
        ],
        "name": "isDocumentSigned",
        "outputs": [
            {
                "internalType": "bool",
                "name": "",
                "type": "bool"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "bytes32",
                "name": "_documentId",
                "type": "bytes32"
            }
        ],
        "name": "pauseSigning",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "bytes32",
                "name": "_documentId",
                "type": "bytes32"
            }
        ],
        "name": "resumeSigning",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "bytes32",
                "name": "_documentId",
                "type": "bytes32"
            }
        ],
        "name": "signDocument",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    }
]
