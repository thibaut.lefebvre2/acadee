import '@/styles/globals.css'
import 'firebase/auth';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import {getAuth, onAuthStateChanged} from "firebase/auth";
import { auth } from "../firebase"

export default function App({ Component, pageProps }) {

  const [user, setUser] = useState(null);
  const router = useRouter();

  useEffect(() => {
    if (router) {
      if (router.route === '/' && router.route === '/register' && router.route === '/login') {
        onAuthStateChanged(auth, (user) => {
          setUser(user);
          if (!user) {
            router.push('/login');
          }
        });
      }
    }

  }, [router]);

  return <Component {...pageProps} />
}
