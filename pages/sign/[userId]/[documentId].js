import { useState, useEffect } from 'react';
import Layout from '@/components/Layout/Layout';
import {doc, getDoc, Timestamp, updateDoc} from 'firebase/firestore';
import { ref, getDownloadURL } from 'firebase/storage';
import moment from 'moment';
import { useRouter } from 'next/router'
moment.locale('en');
import { Document, Page, pdfjs } from "react-pdf";
import {auth, firestore, storage} from "@/firebase";
import { onAuthStateChanged} from "firebase/auth";
import {getStorage} from "firebase/storage";
import dynamic from "next/dynamic";
import Web3 from "web3";
import { contractAbi} from "@/contractABI";
import SignatureLayout from "@/components/Layout/SignatureLayout";


const PageComponent = dynamic(() => import('react-pdf').then((module) => module.Page), { ssr: false });

export default function SignDocument() {
    const router = useRouter()
    const { userId, documentId } = router.query
    const [document, setDocument] = useState(null);
    const [pdfUrl, setPdfUrl] = useState(null);
    const [numPages, setNumPages] = useState(null);
    const [pdf, setPdf] = useState(null);
    const [transactionUrl, setTransactionUrl] = useState("")
    const contractAddress = process.env.NEXT_PUBLIC_CONTRACT_ADDRESS

    useEffect(() => {
        pdfjs.GlobalWorkerOptions.workerSrc =`https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
        if (!document) {
            fetchDocument();
        }
    }, [document, userId, documentId]);

    useEffect(() => {
        loadPdf()
    }, [pdfUrl]);


    useEffect(() => {
        if (document && document.event) {
            setTransactionUrl(process.env.NEXT_PUBLIC_POLYGONSCAN + document.event.transactionId)
        }
    }, [transactionUrl]);


    const fetchDocument = async () => {

        console.log('fetch')
        if (userId && documentId) {
            console.log('fetch2')
            await getDoc(doc(firestore, `users/${userId}/documents/${documentId}`)).then( async (res) => {
                const docData = res.data()
                console.log('docData', docData)

                if (docData) {
                    const pdfPath = `${auth.currentUser.uid}-${docData.name}`
                    const pdfUrl = await getDownloadURL(ref(storage, pdfPath));

                    setPdfUrl(pdfUrl);
                    setDocument(docData);

                    if (docData.event) {
                        console.log(docData.event.transactionId)
                        setTransactionUrl(process.env.NEXT_PUBLIC_POLYGONSCAN + docData.event.transctionId)
                    }
                }

            }).catch(err => { console.error(err)})
        }
    }

    const loadPdf = async () => {
        try {
            if (pdfUrl) {
                const pdfDocument = await pdfjs.getDocument({ url: pdfUrl }).promise
                setPdf(pdfDocument);
                setNumPages(pdfDocument._pdfInfo.numPages);
            }
        } catch (error) {
            alert(error.message);
        }
    }

    const renderPageImages = () => {
        if (!pdf) return null;
        const pages = [];
        for (let i = 1; i <= numPages; i++) {
            pages.push(
                <div key={i} style={{ marginBottom: '1rem' }}>
                    <PageComponent pageNumber={i} pdf={pdf} scale={1.0} />
                </div>
            );
        }

        return pages;
    };

    const signDocument = async () => {

        if (document) {
            const accounts = await window.ethereum.request({ method: "eth_requestAccounts" });
            const web3 = new Web3(window.ethereum);
            const contract = new web3.eth.Contract(contractAbi, contractAddress);

            try {
                const result = await contract.methods.signDocument(document.documentId).send({ from: accounts[0], gasLimit:500000 });

                const event = {
                    transactionId:result.transactionHash,
                    from:result.events.DocumentSigned.returnValues.signer,
                    signedAt:Timestamp.fromDate(new Date())
                }

                // Mettre à jour la base de données Firestore avec la signature de l'utilisateur
                const docRef = doc(firestore, `users/${auth.currentUser.uid}/documents`, documentId);

                await updateDoc(docRef, {
                    event: event
                });

                // Recharger le document pour voir la nouvelle liste de signataires
                fetchDocument();
            } catch (error) {
                console.error(error);
                alert("Erreur lors de la signature du document.");
            }
        }
    };

    return (
        <SignatureLayout>
            <div className="flex w-full gap-5 mb-6">
                <h1 className="text-2xl font-sans text-center w-full mt-20">{ document ? document.name : ''}</h1>
            </div>
            <div className="flex flex-col md:flex-row justify-center w-full">
                <div className="flex flex-col w-full md:w-2/3 gap-5">
                    { !document && <p>Loading...</p>}
                    <div>
                        {
                            pdfUrl &&
                            (<div className="flex flex-col w-full gap-2" >
                                    {
                                        renderPageImages()
                                    }
                                </div>
                            )
                        }
                    </div>
                </div>
                <div className="flex flex-col w-full md:w-1/3 gap-5">
                    <h1 className="text-2xl font-sans">History</h1>
                    <div className="flex flex-col w-full gap-5">
                        {document && !document.event && (<p className="text-sm badge badge-secondary badge-outline">Document not signed yet...</p>)}
                        {document && !document.event && !document.paused && (<button className="btn btn-primary w-full" onClick={signDocument}>Sign document</button>)}
                        {document && document.event && (<p className="w-full text-sm badge badge-info badge-outline">Signed by <span className="font-bold">&nbsp;{document.event.from}&nbsp;</span> at {moment(document.event.signedAt.toDate()).format('LLL')}</p>)}
                        {document && document.event && (<a href={transactionUrl} target="_blank" className="btn btn-info w-full">Show transaction Etherscan</a>)}
                        {document && <a href={pdfUrl} target="_blank" className="btn btn-accent w-full">Upload PDF</a>}
                    </div>


                </div>
            </div>
        </SignatureLayout>
    );
};
