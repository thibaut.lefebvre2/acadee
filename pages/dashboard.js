import React, {useEffect, useState} from 'react'
import Layout from "@/components/Layout/Layout";
import { collection, onSnapshot, query, orderBy, doc, deleteDoc } from 'firebase/firestore';
import Link from 'next/link'
import {firestore} from "@/firebase"
import { textTruncate} from "@/helpers/StringHelpers";
import moment from "moment";
import {auth} from "@/firebase"
import {EyeIcon, TrashIcon} from "@heroicons/react/20/solid";

export default function Dashboard() {

    const [documents, setDocuments] = useState([]);

    useEffect(() => {

        if (auth && auth.currentUser) {
            onSnapshot(query(collection(firestore, `users/${auth.currentUser.uid}/documents`), orderBy('createdAt', 'desc')), (snapshot) => {
                const newDocuments = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
                setDocuments(newDocuments);
            });
        }
    });

    const deleteDocument = async (id) => {
        try {
            const docRef = doc(firestore, `users/${auth.currentUser.uid}/documents`, id);
            await deleteDoc(docRef);
        } catch (error) {
            console.error(error);
            alert("Erreur lors de la suppression du document.");
        }
    };

    return (
        <Layout>
            <div className="flex flex-col md:flex-row w-full gap-2">
                <div className="flex flex-col w-full md:w-1/2">
                    <table className="table w-full text-sm">
                        {/* head*/}
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Signers</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            {
                                documents.map((d) => (
                                    <tr key={d.id}>
                                        <td>{d.id}</td>
                                        <td>{textTruncate(d.name, 24, '***.pdf')}</td>
                                        <td>
                                            <div className="flex flex-col gap-2">
                                                {
                                                    d.signers &&
                                                    d.signers.length > 1 &&
                                                    d.signers.map((s, index) => {
                                                        return (<div key={index} className="badge badge-primary">
                                                            {s}
                                                        </div>)
                                                    })
                                                }
                                            </div>
                                        </td>
                                        <td>{moment(d.createdAt.toDate()).format('LLL')}</td>
                                        <td>
                                            <div className="flex gap-5">
                                                <Link href={`/document/${d.id}`}><EyeIcon className="text-blue-500 w-8 h-8" /></Link>
                                                <button onClick={() => deleteDocument(d.id)}>
                                                    <TrashIcon className="text-red-500 w-8 h-8" />
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                ))
                            }
                        {/* row 1 */}
                        </tbody>
                    </table>
                </div>
            </div>
        </Layout>
    )
}
