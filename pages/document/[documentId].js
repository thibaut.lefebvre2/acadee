import { useState, useEffect } from 'react';
import Layout from '@/components/Layout/Layout';
import {doc, getDoc, Timestamp, updateDoc} from 'firebase/firestore';
import { ref, getDownloadURL } from 'firebase/storage';
import moment from 'moment';
import { useRouter } from 'next/router'
moment.locale('en');
import { Document, Page, pdfjs } from "react-pdf";
import {auth, firestore, storage} from "@/firebase";
import { onAuthStateChanged} from "firebase/auth";
import {getStorage} from "firebase/storage";
import dynamic from "next/dynamic";
import Web3 from "web3";
import { contractAbi} from "@/contractABI";
import { ClipboardDocumentIcon } from "@heroicons/react/20/solid";


const PageComponent = dynamic(() => import('react-pdf').then((module) => module.Page), { ssr: false });

export default function DocumentPage() {
    const router = useRouter()
    const { documentId } = router.query
    const [document, setDocument] = useState(null);
    const [pdfUrl, setPdfUrl] = useState(null);
    const [numPages, setNumPages] = useState(null);
    const [pdf, setPdf] = useState(null);
    const [transactionUrl, setTransactionUrl] = useState("")
    const [copied, setCopied] = useState(false);
    const contractAddress = process.env.NEXT_PUBLIC_CONTRACT_ADDRESS

    const origin =
        typeof window !== 'undefined' && window.location.origin
            ? window.location.origin
            : '';

    console.log(origin)

    useEffect(() => {
        pdfjs.GlobalWorkerOptions.workerSrc =`https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`
        if (!document) {
            onAuthStateChanged(auth, (user) => {
                if (user) {
                    fetchDocument();
                }
            });
        }
    }, [document]);

    useEffect(() => {
        loadPdf()
    }, [pdfUrl]);


    useEffect(() => {
        if (document && document.event) {
            setTransactionUrl(process.env.NEXT_PUBLIC_POLYGONSCAN + document.event.transactionId)
        }
    }, [transactionUrl]);


    const fetchDocument = async () => {
        await getDoc(doc(firestore, `users/${auth.currentUser.uid}/documents/${documentId}`)).then( async (res) => {
            const docData = res.data()

            if (docData) {
                const pdfPath = `${auth.currentUser.uid}-${docData.name}`
                const pdfUrl = await getDownloadURL(ref(storage, pdfPath));

                setPdfUrl(pdfUrl);
                setDocument(docData);

                if (docData.event) {
                    console.log(docData.event.transactionId)
                    setTransactionUrl(process.env.NEXT_PUBLIC_POLYGONSCAN + docData.event.transctionId)
                }
            }

        }).catch(err => { console.error(err)})
    }

    const loadPdf = async () => {
        try {
            if (pdfUrl) {
                const pdfDocument = await pdfjs.getDocument({ url: pdfUrl }).promise
                setPdf(pdfDocument);
                setNumPages(pdfDocument._pdfInfo.numPages);
            }
        } catch (error) {
            alert(error.message);
        }
    }

    const renderPageImages = () => {
        if (!pdf) return null;
        const pages = [];
        for (let i = 1; i <= numPages; i++) {
            pages.push(
                <div key={i} style={{ marginBottom: '1rem' }}>
                    <PageComponent pageNumber={i} pdf={pdf} scale={1.0} />
                </div>
            );
        }

        return pages;
    };

    const handleCopy = () => {
        navigator.clipboard.writeText(`${window.location.origin}/sign/${auth.currentUser.uid}/${documentId}`);
        setCopied(true);
    }

    const signDocument = async () => {

        if (document) {
            const accounts = await window.ethereum.request({ method: "eth_requestAccounts" });
            const web3 = new Web3(window.ethereum);
            const contract = new web3.eth.Contract(contractAbi, contractAddress);

            try {
                const result = await contract.methods.signDocument(document.documentId).send({ from: accounts[0], gasLimit:500000 });

                const event = {
                    transactionId:result.transactionHash,
                    from:result.events.DocumentSigned.returnValues.signer,
                    signedAt:Timestamp.fromDate(new Date())
                }

                // Mettre à jour la base de données Firestore avec la signature de l'utilisateur
                const docRef = doc(firestore, `users/${auth.currentUser.uid}/documents`, documentId);

                await updateDoc(docRef, {
                    event: event
                });

                // Recharger le document pour voir la nouvelle liste de signataires
                fetchDocument();
            } catch (error) {
                console.error(error);
                alert("Erreur lors de la signature du document.");
            }
        }
    };

    const pauseSignature = async () => {
        if (document) {

            try {
                const accounts = await window.ethereum.request({ method: "eth_requestAccounts" });
                const web3 = new Web3(window.ethereum);
                const contract = new web3.eth.Contract(contractAbi, contractAddress);
                const docRef = doc(firestore, `users/${auth.currentUser.uid}/documents`, documentId);

                let result = null
                if (document.paused) {
                    // unpause
                    let result = await contract.methods.resumeSigning(document.documentId).send({ from: accounts[0], gasLimit:500000 });
                    await updateDoc(docRef, {
                        paused: false
                    });
                } else {
                    let result = await contract.methods.pauseSigning(document.documentId).send({ from: accounts[0], gasLimit:500000 });
                    await updateDoc(docRef, {
                        paused: true
                    });
                }

                // Recharger le document pour voir la nouvelle liste de signataires
                fetchDocument();

            } catch(error) {
                console.log(error)
                alert("Error when pausing/resuming the document");
            }
        }
    }

    return (
        <Layout>
            <div className="flex w-full gap-5 mb-6">
                <h1 className="text-2xl font-sans text-center">Document {documentId} - { document ? document.name : ''}</h1>
            </div>
            <div className="flex flex-col md:flex-row">
                <div className="flex flex-col w-full md:w-1/2 gap-5">
                    { !document && <p>Chargement...</p>}
                    <div>
                        {
                            pdfUrl &&
                            (<div className="flex flex-col w-full gap-2" >
                                    {
                                        renderPageImages()
                                    }
                                </div>
                            )
                        }
                    </div>
                </div>
                <div className="flex flex-col w-full md:w-1/2 gap-5">

                    {origin && auth && auth.currentUser && (
                        <div>
                            <label className="block text-gray-800 font-medium mb-1">Share this link to sign the document:</label>
                            <div className="flex items-center">
                                <input
                                    type="text"
                                    className="block w-full rounded-md border-gray-300 py-2 px-4 leading-5 bg-white focus:outline-none focus:bg-white focus:border-gray-500 focus:ring-2 focus:ring-gray-500 focus:ring-opacity-50"
                                    value={`${origin}/sign/${auth.currentUser.uid}/${documentId}`}
                                    readOnly
                                />
                                <ClipboardDocumentIcon onClick={handleCopy} className="h-5 w-5 ml-2 cursor-pointer text-gray-500 hover:text-gray-600" />
                            </div>
                            {copied && <p className="text-green-500 mt-2">Link copied to clipboard</p>}
                        </div>
                    )}

                    <h1 className="text-2xl font-sans">History</h1>
                    <div className="flex flex-col w-full gap-5">
                        {document && !document.event && (<p className="text-sm badge badge-secondary badge-outline">Document not signed yet...</p>)}
                        {document && !document.event && !document.paused && (<button className="btn btn-primary w-full" onClick={signDocument}>Sign document</button>)}
                        {document && !document.paused && !document.event && (<button className="btn btn-error w-full" onClick={pauseSignature}>Pause signature</button>)}
                        {document && document.paused && !document.event && (<button className="btn btn-success w-full" onClick={pauseSignature}>Resume signature</button>)}
                        { document && (<p className="w-full text-sm badge badge-primary badge-outline">ID on blockhain : <span className="font-bold">&nbsp;{document.documentId}</span></p>) }
                        {document && document.event && (<p className="w-full text-sm badge badge-info badge-outline">Signed by <span className="font-bold">&nbsp;{document.event.from}&nbsp;</span> at {moment(document.event.signedAt.toDate()).format('LLL')}</p>)}
                        {document && document.event && (<a href={transactionUrl} target="_blank" className="btn btn-info w-full">Show transaction Etherscan</a>)}
                        {document && <a href={pdfUrl} target="_blank" className="btn btn-accent w-full">Upload PDF</a>}
                    </div>
                </div>
            </div>
        </Layout>
    );
};
