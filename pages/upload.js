import { useState} from 'react';
import { ref, uploadBytesResumable, getMetadata } from "firebase/storage";
import Layout from "@/components/Layout/Layout";
import { getFirestore, collection, addDoc, doc, updateDoc, Timestamp } from 'firebase/firestore';
import {auth, firestore, storage} from "@/firebase";
import Swal from 'sweetalert2'
import { contractAbi} from "@/contractABI";
import Web3 from "web3";
import {useRouter} from "next/router";

const UploadPage = () => {
    const [file, setFile] = useState(null);
    const [uploadProgress, setUploadProgress] = useState(0);
    const [error, setError] = useState('');
    const [emails, setEmails] = useState(['']);
    const [docId, setDocId] = useState('')
    const contractAddress = process.env.NEXT_PUBLIC_CONTRACT_ADDRESS
    const router = useRouter()

    console.log(contractAddress)
    const handleFileChange = (event) => {
        setFile(event.target.files[0]);
    };

    const handleAddEmail = () => {
        setEmails([...emails, '']);
    };

    const handleChangeEmail = (index, value) => {
        const newEmails = [...emails];
        newEmails[index] = value;
        setEmails(newEmails);
    };

    const handleDrop = (event) => {
        event.preventDefault();
        event.stopPropagation();
        const files = event.dataTransfer.files;
        if (files.length > 0) {
            setFile(files[0]);
        }
    };

    const handleDragOver = (event) => {
        event.preventDefault();
        event.stopPropagation();
    };

    const uploadOnChain = async (id, md5Hash) => {
        const accounts = await window.ethereum.request({ method: "eth_requestAccounts" });
        const web3 = new Web3(window.ethereum);
        const contract = new web3.eth.Contract(contractAbi, contractAddress);

        try {
            const importDocumentTx = await contract.methods.importDocument(md5Hash).send({ from: accounts[0] });
            const documentId = importDocumentTx.events.DocumentUploaded.returnValues.documentId;

            const docRef = doc(firestore, `users/${auth.currentUser.uid}/documents`, id);
            await updateDoc(docRef, {
                documentId: documentId
            }).then(res => {
                router.push(`/document/${id}`);
            });
        } catch (error) {
            console.error(error);
            alert("Erreur lors de l'import du document.");
        }
    };

    const handleUpload = () => {
        if (file === null) {
            Swal.fire({
                title: 'Error!',
                text: 'Please select file',
                icon: 'error',
                confirmButtonText: 'OK'
            })
            return;
        }
        setError('');
        // Create a root reference
        const fileRef = ref(storage, `${auth.currentUser.uid}-${file.name}`);
        const uploadTask = uploadBytesResumable(fileRef, file);

        uploadTask.on('state_changed', (snapshot) => {
            const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
            setUploadProgress(progress);
        }, (error) => {
            setError(error.message);
        }, async () => {
            const firestore = getFirestore();
            const metadata = await getMetadata(fileRef);
            const createdAt = Timestamp.fromDate(new Date(metadata.timeCreated));

            const data = {
                name: file.name,
                md5Hash:metadata.md5Hash,
                createdAt,
            };
            await addDoc(collection(firestore, `users/${auth.currentUser.uid}/documents`), data).then(res => {
                setDocId(res.id)
                uploadOnChain(res.id, data.md5Hash)
            });




        });
    };

    const handleSendEmails = async () => {
        const docRef = doc(firestore, `users/${auth.currentUser.uid}/documents`, docId);
        await updateDoc(docRef, {
            signers: emails
        }).then(res => {
            Swal.fire({
                title: 'Success!',
                text: 'Good Job',
                icon: 'success',
                confirmButtonText: 'OK'
            })
        })
    }

    return (
        <Layout>
            <div className="flex flex-col gap-5 w-full">
                {error && <p>{error}</p>}

                <div onDrop={handleDrop}
                     onDragOver={handleDragOver}
                     className="flex flex-col gap-2 justify-center items-center h-96 rounded-3xl border-gray-200
                             border-dashed border-2"
                >
                    {!file && (
                        <div className="flex flex-col gap-2 w-full">
                            <p className="font-sans font-bold text-xl text-center"><span className="font-bold uppercase">Drag and drog file here</span></p>
                        </div>
                    )}
                    {file && (
                        <div className="flex flex-col gap-2 w-full">
                            <p className="font-sans font-bold text-xl text-center"><span className="font-bold">{file.name}</span></p>
                            <progress className="progress progress-success w-5/6 mx-auto" value={uploadProgress} max="100"></progress>
                            <div className="flex flex-row-reverse w-full p-12">
                                <button className="btn btn-primary" onClick={handleUpload}>Upload</button>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </Layout>
    );
};

export default UploadPage;