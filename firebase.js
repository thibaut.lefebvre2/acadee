// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getAuth} from "firebase/auth"
import {getFirestore} from "firebase/firestore";
import {collection} from "firebase/firestore";
import {getStorage} from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const firebaseConfig = {
    apiKey: "AIzaSyA_CVLziS1k2b8zk1dMTcD1VC3ChRcFmNo",
    authDomain: "signature-elect.firebaseapp.com",
    databaseURL: "https://signature-elect-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "signature-elect",
    storageBucket: "signature-elect.appspot.com",
    messagingSenderId: "196024424622",
    appId: "1:196024424622:web:41c4f70513972bda9b353f",
    measurementId: "G-0S7L2RERLL"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const firestore = getFirestore(app);
export const auth = getAuth(app);

export const storage = getStorage(app)