const hre = require("hardhat");

async function main() {

  const DocumentSigner = await hre.ethers.getContractFactory("DocumentSigner")
  const documentSigner = await DocumentSigner.deploy()

  await documentSigner.deployed()

  console.log(
    `DocumentSigner deployed to ${documentSigner.address}`
  );
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
